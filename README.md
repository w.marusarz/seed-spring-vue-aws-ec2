## Readme covers

- [Prerequisites](#prerequisites) - what you need to install to run project
- [Running on localhost](#running-on-localhost) - how to start application, using build tools or docker
- [Setup required for EC2](#setup-required-for-ec2) - how to create AWS account and run EC2
- [Gitlab setup](#gitlab-setup) - how to store your secrets in gitlab
- [Elastic IP address assigned to EC2 instance](#elastic-ip-address-assigned-to-ec2-instance) - how to run application
  on EC2 and access it using IP address
- [Domain assigned to EC2 instance](#domain-assigned-to-EC2-instance) - how to run application on EC2, how to access it
  using domain name, and how to configure SMTP server.

## About

This is a seed project that allows to run API (Spring Boot) and UI (Vue) applications at AWS EC2, deployed from gitlab,
using ansible, with a minimal setup required. Applications can be accessed by IP or by domain name.

Spring Boot application is written in Kotlin. For builds, it uses gradle with plugins: *kotlinter*, *ben-manes.versions*
and *test-fixtures*. Testcontainers is used to test database setup. Spring Security has configured CORS and Basic
authentication - *based on requirements, should be changed to other authentication method ex. JWT*

Deployment process:

1. Configure EC2
1. Build Spring Boot and Vue application
1. Build docker images and pushes them to AWS ECR
1. Start docker containers on EC2
1. All services (spring boot, vue, mongo) run on a single EC2 instance. *If required, please take care about database
   backups.*
1. Traefik is used as a reverse proxy, to route https requests to required containers by domain name.

That's how the application looks in a browser. When the page is opened it sends a request to BE to verify connection.
Additionally, if domain and SMTP server is configured, email can be sent.

![Application](./readme/app.png "Application")

## Prerequisites

### Before you start, install at your machine

1. JDK 8+
2. NPM
3. NodeJS
4. Docker
3. docker-compose

___

## Running on localhost

### Run for development purposes

Use your favourite IDE to start application or execute commands in console

1. Run mongodb service and verify you can log in to mongodb shell:
   ```shell
   mongo localhost:27017
   ```
1. Run Spring Application
   ```shell
   cd seed-spring && ./gradlew bootRun
   ```
1. Run Vue Application
   ```shell
   npm install --prefix seed-vue
   npm run --prefix seed-vue serve
   ```

As a result, you should be able to open application at `localhost:8081` <br>
Request to BE should succeed, it means no errors are displayed, and your local IP with timestamp is displayed.

![Local IP with timestamp - run on localhost](./readme/local-app.png "Local IP with timestamp - run on localhost")

### Run docker containers locally

#### Additional setup required

1. Create group docker
   ```shell
   sudo groupadd docker
   ```
1. Add currently logged user to docker group
   ```shell
   sudo usermod -aG docker $USER
   ```
1. Logout and Login
1. Verify that user is added to docker group
   ```shell
   groups | grep docker
   ```

*Note: Make sure that mongodb service is stopped, to allow run docker mongodb container*

To run docker containers locally, just execute script:

```shell
sh run-seed-local.sh
```

Scripts `run-seed-local.sh` builds Spring Boot and Vue applications, builds images and runs them.<br>
When script is executed, verify that everything works as expected

```shell
docker ps | grep seed
```

Verify that three containers are up and running:<br>
*seed-spring-vue-aws_seed-vue*, *seed-spring-vue-aws_seed-spring*, *mongo*

As a result, you should also be able to open application at `localhost:8081` <br>
Request to BE should succeed, it means no errors are displayed, and your local IP with timestamp is displayed.

![Local IP with timestamp - run on localhost](./readme/local-app.png "Local IP with timestamp - run on localhost")

#### Note: Sending emails from localhost

SMTP configuration is mentioned later, in [SMTP config for emails](#smtp-config-for-emails), but if you want to send
email from localhost, create `application-secret.yml` and run application with spring profile: `secret`. File is listed
in `.gitignore` and won't be available in the repository to hide your secrets. Add to `application-secret.yml` file:

```
spring:
  mail:
    username: 'your-smtp-server-username'
    password: 'your-smtp-server-password'
```

___

## Setup required for EC2

If you want to make your application available online, you can deploy it to AWS EC2. This requires minimal
configuration, described below.

**Note: All docker containers (Spring Boot, Vue, Mongo) will be run on a single EC2 instance. It is not recommended for
production applications, but it is enough for fast prototyping. If required, please take care about database backups.**

### Setup EC2

Two access modes to your application deployed on EC2 are available:

1. [Elastic IP address assigned to EC2 instance](#elastic-ip-address-assigned-to-ec2-instance)
1. [Domain assigned to EC2 instance](#domain-assigned-to-EC2-instance)

You can decide how to access your application later. For now, some **AWS setup is required**

1. Login to AWS management console *(create account if you don't have one)*
1. Select region *(it will be used later in configuration file)*
1. Create new EC2 instance *(ex.: Ubuntu Server 20.04 LTS 64-bit (x86), t2.micro)*<br>
   Note: *Select instance with option: Free tier eligible. It means 750 hours each month per year<br>
   Read more about [AWS Pricing](https://aws.amazon.com/ec2/pricing/). Monitor your spendings in AWS console.*
1. For sake of seed project, use default configuration of EC2 instance
1. When prompted, select existing or create a new key pair. Save it to *~/workspace/secret/seed.pem*
   *(It will be required to log-in with ssh to EC2 instance)*
1. Create Elastic IP address and associate it with EC2 instance *(it will be used later in configuration file)*
1. Verify that EC2 is up and running. Connect to EC2 using ssh
   ```shell
   ssh -i ~/workspace/secret/seed.pem ubuntu@[ELASTIC-IP-HERE]
   ```
1. Additionally, configure Security Group assigned to launched EC2.<br>
   Enable Inbound Rules:
    - HTTP 443 *expose https port - required only if you want to access your application using domain*<br>
    - TCP 8080 *expose port for Spring Boot application (required if application accessed by IP)*<br>
    - TCP 8081 *temporarily expose port for Vue application (required if application accessed by IP)*<br>
    - TCP 8082 *expose port (only from your IP) for reverse proxy*<br>
    - TCP 8083 *expose port (only from your IP) for Spring Boot Actuator where you can monitor your application*<br>

That's how it should look like in AWS: (TODO wojtek update image both for github to hide ip and here to show different
config)

![EC2 Security Group Inbound Rules](./readme/inbound-rules.png "EC2 Security Group Inbound Rules")

___

## Gitlab setup

### Environment variables

**Important Note:** **To hide your secrets do not store them in any file in repository. Use gitlab environment variables
instead to protect yor secrets. For the purpose of this project, select CI/CD->Variables and create variables as below**

![Gitlab environment variables](./readme/gitlab-env.png "Gitlab environment variables")

**Note: Variables PB_CONFIG_IP, PB_CONFIG_DOMAIN and SSH_KEY_EC2 are of File type.**

SSH_KEY_EC2 is a private key used to access EC2 with SSH explained in paragraph [Setup EC2](#setup-ec2)

PB_CONFIG_IP is a configuration required to access application by IP, it should contain entries as below. Replace '
changeme' values with your account details.

```
aws_access_key_id: "changeme"         # format: "AAAAAAAAAAAAAAAAAAAA"
aws_secret_access_key: "changeme"     # format: "aaaaaaaaaaaaaaaaaaaa+aaaaaaaaaaaaaaaaaaa"
aws_region: "changeme"                # format: "cn-location-1"
aws_account_id: "changeme"            # format: "000000000000"
cors_allowed_origin: "changeme"       # format: "http://89.187.123.456:8081" or https://yourdomain.com
vue_app_api_address: "changeme"       # format: "http://89.187.123.456:8080" - your Elastic IP
vue_app_basicAuthUsername: "changeme" # Used to authenticate request using basic authentication
vue_app_basicAuthPassword: "changeme" # Used to authenticate request using basic authentication
seed_mail_username: "will-be-ignored-for-ip"
seed_mail_password: "will-be-ignored-for-ip"
traefik_host: "will-be-ignored-for-ip"
api_host: "will-be-ignored-for-ip"
ui_host: "will-be-ignored-for-ip"
issuer_email: "will-be-ignored-for-ip"
```

PB_CONFIG_DOMAIN is a configuration required to access application by domain, it should contain entries as below.
Replace 'changeme' values with your account details.

```
aws_access_key_id: "changeme"         # format: "AAAAAAAAAAAAAAAAAAAA"
aws_secret_access_key: "changeme"     # format: "aaaaaaaaaaaaaaaaaaaa+aaaaaaaaaaaaaaaaaaa"
aws_region: "changeme"                # format: "cn-location-1"
aws_account_id: "changeme"            # format: "000000000000"
cors_allowed_origin: "changeme"       # format: "https://yourdomain.com"
vue_app_api_address: "changeme"       # format: "https://api.yourdomain.com" - your domain prefixed with api subdomain
vue_app_basicAuthUsername: "changeme" # Used to authenticate request using basic authentication
vue_app_basicAuthPassword: "changeme" # Used to authenticate request using basic authentication
seed_mail_username: "changeme"        # Set if you want to enable email sending - Read more in section: [SMTP config for emails](#smtp-config-for-emails)
seed_mail_password: "changeme"        # Set if you want to enable email sending - Read more in section: [SMTP config for emails](#smtp-config-for-emails)
traefik_host: "traefik.changeme"      # format: "traefik.yourdomain.com"
api_host: "api.changeme"              # format: "api.yourdomain.com"
ui_host: "changeme"                   # format: "yourdomain.com"
issuer_email: "changeme@yourdomain.com"
```

___

## Elastic IP address assigned to EC2 instance

### Deploy application

Before you deploy application on EC2, make sure that EC2 instance is up and running ([Setup EC2](#setup-ec2)) and gitlab
environment variables are configured ([Gitlab setup](#gitlab-setup))

To deploy application, just run pipeline, and execute each stage:

![Gitlab pipelines](./readme/pipelines-ip.png "Gitlab pipelines")

Stage **Build** builds both API and UI. Stage **Setup** is enough to run only once, just to set up EC2. Stage **Image**
builds docker images end pushes them to ECR. Stage **Deploy** has two jobs. To access application by IP, run job
**deploy:IP**. This job downloads images from ECR and starts whole application.

#### Verify application running

1. Login to EC2
   ```shell
   ssh -i ~/workspace/secret/seed.pem ubuntu@89.187.123.456 Note: replace ip address with your Elastic IP
   ```
1. When logged in, verify that docker images (`seed-vue`, `seed-vue`, `seed-mongo`) are up and running.
   ```shell
   docker ps
   ```
1. Open application in a browser: http://[ELASTIC-IP]:8081 Note: *replace [ELASTIC-IP] with your Elastic IP*

### MongoDB database restarts

Docker images with MongoDB are not restarted to prevent data loss. If you want to drop and restart database,
see [Database cleanup](#database-cleanup)

## Domain assigned to EC2 instance

### Domain setup

1. If you don't own a domain, you can buy one.<br>
   I've selected http://domain.com provider because it was the cheapest one.<br>
   **Important Note:** *Turn off domain auto-renewal if you don’t want to be charged every year - price for renewal is
   usually higher*
1. Create email account - *it will be required when https ca certificates will be generated*
1. Create cloudflare account https://www.cloudflare.com/ *(free account available)*.<br>
   Log-in and add DNS entries for your domain. Replace CNAME record with your own domain.

   ![Cloudflare dns setup](./readme/dns.png "Cloudflare dns setup")

1. Allow cloudflare to manage your DNS. Cloudflare provides detailed instruction how to do this, like below

   ![Cloudflare domain management](./readme/cf-manage.png "Cloudflare domain management")

1. Point to cloudflare nameservers, at domain.com it can be done like below:

   ![Point to cloudflare nameservers](./readme/domain.png "Point to cloudflare nameservers")

1. In cloudflare enable http to https redirection and select option to always use https

Note: *If you're done, you need to wait even up to 24h (usually it is faster), for changes to take effect.*

### SMTP config for emails

Create a mailgun account if you don't have one. Note: *phone number verification is required*. For development purposes,
mailgun offers a free plan - emails to 5 verified email addresses can be sent. No domain configuration is required for
the free plan, because the sandbox environment is used.

From menu: *Sending > Domains* select your sandbox domain, select *SMTP* option, and get your credentials. Value  
*Username* will be used to set *seed_mail_username* and *Default password* will be used to set *seed_mail_password*
in *pb-config.yml*
Other values are configured in *application.yml* and don't have to be changed.

![SMTP configuration](./readme/smtp.png "SMTP configuration")

The paid plan may be used if needed, but it requires additional domain configuration. Mailgun provides simple and
detailed instructions on how to do this, and it is not in the scope of this seed project.

### Deploy application

Before you deploy application on EC2, make sure that EC2 instance is up and running ([Setup EC2](#setup-ec2)) and gitlab
environment variables are configured ([Gitlab setup](#gitlab-setup))

To deploy application, just run pipeline, and execute each stage:

![Gitlab pipelines](./readme/pipelines-domain.png "Gitlab pipelines")

Stage **Build** builds both API and UI. Stage **Setup** is enough to run only once, just to set up EC2. Stage **Image**
builds docker images end pushes them to ECR. Stage **Deploy** has two jobs. To access application by IP, run job
**deploy:IP**. This job downloads images from ECR and starts whole application.

#### Verify application running

1. Login to EC2
   ```shell
   ssh -i ~/workspace/secret/seed.pem ubuntu@89.187.123.456 Note: replace ip address with your Elastic IP
   ```
1. When logged in, verify that docker images (`seed-vue`, `seed-vue`, `seed-mongo`) are up and running.
   ```shell
   docker ps
   ```
1. Open application in a browser: http://your-domain.com

#### Note: after everything, block ports 8080,8081 in Inbound Rules - make them available only from your IP

### MongoDB database restarts

Docker images with MongoDB are not restarted to prevent data loss. If you want to drop and restart database,
see [Database cleanup](#database-cleanup)
___

## Database cleanup

Docker container with MongoDB is started only once, during the first deploy to prevent data loss between consecutive
deploys. This behavior is controlled by `~/very-first-run` file which is created on EC2 after first deploy. If you want
to restart database, you need to override this default behavior. Just run pipeline and set `$DB_SETUP` environment
variable in gitlab to `true`.

**Note** All your data will be lost, so be very careful.

![Drop database and restart](./readme/db-setup.png "Drop database and restart")

---

## Troubleshooting

#### Backup & restore

To prevent data loss, you can take care of backups. To make backup, allow accessing port _27017_ from your IP address -
edit EC2 inbound rules as already described.

```shell
mongodump --host="89.187.123.456:27017"" --db="seed" --out="./seed-db/"
```

If you would accidentally remove your database, you can restore it with:

```shell
mongorestore --host="89.187.123.456:27017"  ./seed-db/
```

Note: Replace ip address with your elastic IP assigned to EC2

#### Certificates generation

Note: For development purposes, in `playbook-run.yml` uncomment acme-staging ca server, as shown below:

```
"--certificatesresolvers.myresolver.acme.caserver=https://acme-staging-v02.api.letsencrypt.org/directory"
```

To re-generate certificates, at EC2 remove the old one, and at your machine run `playbook-run.yml` once again. See
traefik logs for verification:

```shell
sudo rm /home/ubuntu/letsencrypt/acme.json
docker logs seed-traefik
```

If everything works fine - no errors should be displayed

#### Troubles with VPN

If you are using VPN, and you run application on localhost with docker images, you may see an error

```shell
Creating network "seed-spring-vue-aws-ec2_default" with the default driver
ERROR: could not find an available, non-overlapping IPv4 address pool among the defaults to assign to the network
```

To make it work, just disconnect from VPN
